/*
Navicat MySQL Data Transfer

Source Server         : 10.0.104.50
Source Server Version : 50720
Source Host           : 10.0.104.50:3306
Source Database       : phone_weixin

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2017-12-20 11:56:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for filted_msg
-- ----------------------------
DROP TABLE IF EXISTS `filted_msg`;
CREATE TABLE `filted_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_title` varchar(255) DEFAULT NULL,
  `msg_digest` varchar(255) DEFAULT NULL,
  `read_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21063 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `biz` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `nickid` varchar(255) DEFAULT NULL,
  `nickdesc` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `moditime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for mpaccount
-- ----------------------------
DROP TABLE IF EXISTS `mpaccount`;
CREATE TABLE `mpaccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `biz` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `metavalue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for msg
-- ----------------------------
DROP TABLE IF EXISTS `msg`;
CREATE TABLE `msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_title` varchar(255) DEFAULT NULL,
  `msg_link` varchar(255) DEFAULT NULL,
  `publish_time` bigint(20) DEFAULT NULL,
  `modi_time` bigint(20) DEFAULT NULL,
  `read_num` int(11) DEFAULT NULL,
  `like_num` int(11) DEFAULT NULL,
  `reward_total_count` int(11) DEFAULT NULL,
  `msg_idx` varchar(255) DEFAULT NULL,
  `msg_mid` varchar(255) DEFAULT NULL,
  `msg_biz` varchar(255) DEFAULT NULL,
  `msg_source_url` longtext,
  `msg_cover` varchar(255) DEFAULT NULL,
  `msg_digest` varchar(255) DEFAULT NULL,
  `is_fail` bit(1) DEFAULT NULL,
  `copyright_stat` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21342 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for new_msg
-- ----------------------------
DROP TABLE IF EXISTS `new_msg`;
CREATE TABLE `new_msg` (
  `id` int(11) NOT NULL DEFAULT '0',
  `msg_title` varchar(255) DEFAULT NULL,
  `msg_digest` varchar(255) DEFAULT NULL,
  `read_num` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
